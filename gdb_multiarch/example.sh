#! /bin/bash
docker build -t toto -f arm32v7-alpine .
docker run -p 127.0.0.1:1234:1234 -it toto

# execution de gdb dans un autre terminal
#
# docker cp <container_id>:/path/in/container /path/in/host
# 
# gdb mon_binaire
# 
#  target remote :1234
#
